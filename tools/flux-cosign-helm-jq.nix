{ pkgs ? import <nixpkgs> { }
  , pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:

pkgs.dockerTools.buildImage {
  name = "flux-jq-cosign-helm-kubectl-terraform";
  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [
      pkgs.bash
      pkgs.gnugrep
      pkgs.coreutils
      pkgs.cosign
      pkgs.crane
	    pkgs.fluxcd
 	    pkgs.jq
      pkgs.kubernetes-helm
      pkgs.kubectl
    ];
    pathsToLink = [ "/bin" ];
  };

  config = {
    #WorkingDir = "/app";
    Volumes = { "/tmp" = { }; };
    Env = [ "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt" ];
  };
}