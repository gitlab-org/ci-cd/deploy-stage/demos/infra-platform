#!/bin/bash
# Copy the image from this project to the public infra+platform container registry

## Crane does not support gzipped containers 
crane auth login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
crane copy $1 $2
