# Platform management with GitLab

The platform directory contains a Terraform project to show how GitLab can be used to fully automate project lifecycle management with GitLab. This automation allows compliance and standardization. 

The platform project will likely need to be customized a lot for every company. This is just a very simple example.

## Assumptions

- The application project is managed fully by the app development team. Including project creation. As a result, the applicaiton project already exists when the project is enrolled to the platform.
- The delivery project might be managed by any team, but it's owned by the platform team. The delivery project is created through the automation.

## Platform scope

The platform 

- creates a delivery project
- sets up Flux to track an `entrypoint` OCI image in the delivery project's container registry
- creates a namespace, service account, RBAC in every cluster for the application
- sets up a deploy key in the clusters to retrieve containers from the delivery project
- sets up cosign in the delivery project and in the cluster (public key) to verify containers
- sets up environment variables to simplify agent usage in the delivery project
