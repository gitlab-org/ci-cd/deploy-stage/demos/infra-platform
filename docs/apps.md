# Installed applications

The `manifests` directory is watched by Flux and stores all the Kubernetes manifests we want to deploy. 
The directory has sub-directories per environment (`dev` and `prod`). 
The other directories organize the resources by topic. e.g. `core` includes the manifests deployed to all the clusters. 
The `core` directory is referenced from the environment specific directories.

The `core` directory adds RBAC for CI and user access and installs Bitnami Sealed Secrets.
Sealed Secrets will be used to store encrypted secrets in the repository when the platform requires it.
Once Sealed Secrets is installed, its public keys need to be retrieved and sommitted to the repo.

The [platform](./platform.md) directory generates application specific files for every environment, and stores them under the manifests directory.