# Infrastructure provisioning

## Setup

1. Set up OIDC access for GitLab to GCP
1. Make sure that the OIDC service account has the necessary roles to
   - create the GCP resources
   - access the created GKE cluster as admin (requires the https://cloud.google.com/iam/docs/understanding-roles#container.clusterAdmin role)
1. Create the environment variables:
   - `GCP_PROJECT_ID`
   - `GCP_REGION`
   - `GCP_SERVICE_ACCOUNT`
   - `GCP_WORKLOAD_IDENTITY_PROVIDER`
1. Create a Group Access Token (or Personal Access Token for a personal namespace) with `api` and `write_repository` rights and `Maintainer` role. Save the token as the `TF_VAR_GITLAB_TOKEN` environment variable.

### Setting up Flux in the repository

We want to install Flux with Terraform. While Flux has a Terraform provider, it's hard to use as Flux is meant to synchronize itself, so Terraform and Flux will fight for authority. Instead of using the Flux provider, we are going to install Flux with the kustomize provider. Flux will use the same manifests to keep itself in sync with the repository. For this setup we need to add the Flux manifests to the repository.

The following commands need to be run for every environment (replace `<environment name>` with your environment name. e.g. `prod`)

1. Run `flux install --export > manifests/<environment name>/flux-system/gotk-components.yaml` to add the Flux core manifests to the repository.
1. Run

   ```bash
   flux create source git flux-system \
   --url=https://gitlab.com/gitlab-org/ci-cd/deploy-stage/demos/infra-platform.git \
   --branch=main \
   --secret-ref=flux-system \
   --interval=10m0s \
   --export > manifests/<environment name>/flux-system/gotk-sync.yaml
   ```

   to generate the manifests for Flux to watch itself in this GitRepository
1. Run

   ```bash
   flux create kustomization flux-system \
   --source=GitRepository/flux-system \
   --path="./manifests/<environment name>" \
   --prune=true \
   --interval=10m0s \
   --wait=true \
   --health-check-timeout=3m \
   --export >> manifests/<environment name>/flux-system/gotk-sync.yaml
   ```

   to generate the manifests for Flux to update itself from the manifests we just exported.
1. Create `manifests/<environment name>/flux-system/kustomization.yaml` with the following content

   ```yaml
   apiVersion: kustomize.config.k8s.io/v1beta1
   kind: Kustomization
   resources:
   - gotk-components.yaml
   - gotk-sync.yaml
   ```

   to use both files in the Kustomize provider and later by Flux.

### OIDC setup

- https://cloud.google.com/iam/docs/workload-identity-federation-with-deployment-pipelines#gitlab-saas
- https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/index.html