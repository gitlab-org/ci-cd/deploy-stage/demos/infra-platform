variable "GITLAB_TOKEN" {
  type = string
}

variable "CI_REGISTRY" {
  type = string
}

variable "CI_PROJECT_ID" {
  type = string
}