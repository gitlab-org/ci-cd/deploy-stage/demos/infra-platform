terraform {
  required_version = ">= 0.12.19"
  backend "http" {
  }
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = ">= 16.4.0, <17.0.0"
    }
  }
}

provider "gitlab" {
    token = var.GITLAB_TOKEN
}
