## Cosign for Dev ##

module "cosign_dev" {
  source = "../cosign"
}

# Store the private key as a protected environment variable
resource "gitlab_project_variable" "COSIGN_PRIVATE_KEY_dev" {
  project   = var.app_project_id
  environment_scope = "dev/*"
  variable_type = "file"
  key       = "COSIGN_PRIVATE_KEY"
  value     = module.cosign_dev.private_key
  protected = true
  masked = false
}

# Store the password as a protected, masked environment variable
resource "gitlab_project_variable" "COSIGN_PASSWORD_dev" {
  project   = var.app_project_id
  environment_scope = "dev/*"
  key       = "COSIGN_PASSWORD"
  value     = module.cosign_dev.password
  protected = true
  masked = true
}

# Create sealed secret for development
module "dev_secrets" {
  source = "../write_file"

  command = "kubectl create secret generic --namespace=${var.namespace} cosign-key --from-file=dev.pub=${module.cosign_dev.public_file} --from-file=charts.pub=${path.root}/${module.cosign_charts.key_prefix}.pub --dry-run=client -o yaml | kubeseal --cert ${path.root}/../sealed-secret.dev.pem --format=yaml"
}

# Set up container registry access in a sealed secret for dev
module "delivery_secret_dev" {
  source = "../write_file"

  command = "kubectl create secret docker-registry gitlab-${local.dev_namespace}-delivery-token --namespace=${local.dev_namespace} --docker-username=${var.delivery_docker_credentials.username} --docker-password=${var.delivery_docker_credentials.password} --docker-server=${var.docker_registry_url} --dry-run=client -o yaml | kubeseal --cert ${path.root}/../sealed-secret.dev.pem --format=yaml >> ${path.module}/delivery_secret_dev.yaml"
}

# Concatenate all the generated YAML resources for prod and dev separately
locals {
  dev_namespace = "${var.namespace}"
  dev_namespace_sa = templatefile("${path.module}/namespace_service_account.yaml.tftpl", { 
    namespace_name=local.dev_namespace,
    sa_name=var.app_project_slug,
    container_registry="oci://${var.docker_registry_url}/${var.app_project_full_slug}/entrypoint/dev"
    })
  dev_rendered_resources = join("---\n", [local.dev_namespace_sa, module.delivery_secret_dev.content, module.dev_secrets.content])
}

# Store the production YAML
resource "gitlab_repository_file" "dev" {
  project        = var.gitops_project_id
  branch         = var.branch
  file_path      = "${var.dev_flux_path}/project-${var.app_project_slug}.yaml"
  content        = base64encode("${local.dev_rendered_resources}")
  commit_message = "Initialize project ${var.app_project_name} dev environment in the cluster"
  overwrite_on_create = true
}
