## Cosign for charts ##

module "cosign_charts" {
  source = "../cosign"
}

# Store the private key as a protected environment variable
resource "gitlab_project_variable" "COSIGN_PRIVATE_KEY_charts" {
  project   = var.app_project_id
  environment_scope = "*"
  key       = "COSIGN_PRIVATE_KEY"
  value     = module.cosign_charts.private_key
  variable_type = "file"
  protected = true
  masked = false  ## Masked is not allowed
}

# Store the password as a protected, masked environment variable
resource "gitlab_project_variable" "COSIGN_PASSWORD_charts" {
  project   = var.app_project_id
  environment_scope = "*"
  key       = "COSIGN_PASSWORD"
  value     = module.cosign_charts.password
  protected = true
  masked = true
}
