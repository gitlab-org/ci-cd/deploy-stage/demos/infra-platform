## Cosign for Production ##
module "cosign_prod" {
  source = "../cosign"
}

# Store the private key as a protected environment variable
resource "gitlab_project_variable" "COSIGN_PRIVATE_KEY" {
  project   = var.app_project_id
  environment_scope = "production/*"
  key       = "COSIGN_PRIVATE_KEY"
  value     = module.cosign_prod.private_key
  variable_type = "file"
  protected = true
  masked = false  ## Masked is not allowed
}

# Store the password as a protected, masked environment variable
resource "gitlab_project_variable" "COSIGN_PASSWORD" {
  project   = var.app_project_id
  environment_scope = "production/*"
  key       = "COSIGN_PASSWORD"
  value     = module.cosign_prod.password
  protected = true
  masked = true
}

# Create sealed secret for production
module "prod_secrets" {
  source = "../write_file"

  command = "kubectl create secret generic --namespace=${var.namespace} cosign-key --from-file=prod.pub=${module.cosign_prod.public_file} --from-file=charts.pub=${module.cosign_charts.public_file} --dry-run=client -o yaml | kubeseal --cert ${path.root}/../sealed-secret.prod.pem --format=yaml"
}

# Set up container registry access in a sealed secret for prod
module "delivery_secret" {
  source = "../write_file"

  command = "kubectl create secret docker-registry gitlab-${var.namespace}-delivery-token --namespace=${var.namespace} --docker-username=${var.delivery_docker_credentials.username} --docker-password=${var.delivery_docker_credentials.password} --docker-server=${var.docker_registry_url} --dry-run=client -o yaml | kubeseal --cert ${path.root}/../sealed-secret.prod.pem --format=yaml"
}

# Concatenate all the generated YAML resources for prod and dev separately
locals {
  prod_namespace_sa = templatefile("${path.module}/namespace_service_account.yaml.tftpl", { 
    namespace_name=var.namespace, 
    sa_name=var.app_project_slug,
    container_registry="oci://${var.docker_registry_url}/${var.app_project_full_slug}/entrypoint/production"
    })
  prod_rendered_resources = join("---\n", [local.prod_namespace_sa, module.delivery_secret.content, module.prod_secrets.content])
}

# Store the production YAML
resource "gitlab_repository_file" "production" {
  project        = var.gitops_project_id
  branch         = var.branch
  file_path      = "${var.prod_flux_path}/project-${var.app_project_slug}.yaml"
  content        = base64encode("${local.prod_rendered_resources}")
  commit_message = "Initialize project ${var.app_project_name} prod environment in the cluster"
  overwrite_on_create = true
}
