variable "gitops_project_id" {
  type = number
}

variable "branch" {
  type = string
  default = "main"
}

variable "app_project_id" {
  type = number
}

variable "app_project_name" {
  type = string
}

variable "app_project_slug" {
  type = string
}

variable "app_project_full_slug" {
  type = string
}

variable "namespace" {
  type = string
}

variable "prod_flux_path" {
    type = string
    default = "manifests/flux-system"
}

variable "dev_flux_path" {
    type = string
    default = "manifests/flux-system"
}

variable "docker_registry_url" {
  type = string
}

variable "delivery_docker_credentials" {
  type = object({
    username    = string
    password = string
  })
  sensitive = true
}
