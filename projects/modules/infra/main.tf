terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = ">= 16.3.0, <17.0.0"
    }
  }
}
