terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = ">= 16.3.0, <17.0.0"
    }
  }
}

# Create the delivery project
resource "gitlab_project" "current_project" {
    name = var.name
    namespace_id = var.namespace_id
    path = var.path
    description = var.description
    visibility_level = var.visibility_level
    pages_access_level  = var.pages_access_level
    shared_runners_enabled = var.shared_runners_enabled
    wiki_enabled = var.wiki_enabled
    snippets_enabled = var.snippets_enabled
    packages_enabled = var.packages_enabled
    container_registry_access_level  = var.container_registry_access_level 
}

# Set up a deploy token for container registry access
resource "gitlab_deploy_token" "project" {
  project    = gitlab_project.current_project.id
  name       = "Cluster container access"
  username   = "cluster-token"

  scopes = ["read_registry"]
}

# Save the agent "slug" for easy access in CI
resource "gitlab_project_variable" "agent_kubecontext" {
  project   = gitlab_project.current_project.id
  environment_scope = "production/*"
  key       = "AGENT_KUBECONTEXT"
  value     = var.production_agent_slug
  protected = false
  masked = false
}

resource "gitlab_project_variable" "agent_kubecontext_dev" {
  project   = gitlab_project.current_project.id
  environment_scope = "dev/*"
  key       = "AGENT_KUBECONTEXT"
  value     = var.dev_agent_slug
  protected = false
  masked = false
}
