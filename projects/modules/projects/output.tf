output "project_path" {  
  value = gitlab_project.current_project.web_url
}

output "project_path_slug" {
  value = gitlab_project.current_project.path
}

output "project_id" {  
  value = gitlab_project.current_project.id
}

output "project_name" {
  value = gitlab_project.current_project.name
}

output "path_with_namespace" {
  value = gitlab_project.current_project.path_with_namespace
}

output "cluster_token" {
  value = {
    username       = "${gitlab_deploy_token.project.username}"
    password       = "${gitlab_deploy_token.project.token}"
  }
  sensitive = true
}
