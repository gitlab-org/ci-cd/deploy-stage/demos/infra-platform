variable "name" {
  type = string
}

variable "path" {
  type = string
}

variable "description" {
  type = string
  default = ""
}

variable "pages_access_level" {
  type = string
  default = "private"
}

variable "visibility_level" {
  type = string
  default = "private"
}

variable "shared_runners_enabled" {
  type = bool
  default = true
}

variable "wiki_enabled" {
  type = bool
  default = false
}

variable "snippets_enabled" {
  type = bool
  default = false
}

variable "packages_enabled" {
  type = bool
  default = false
}

variable "container_registry_access_level" {
  type = string
  default = "private"
}

variable "production_agent_slug" {
  type = string
}

variable "dev_agent_slug" {
  type = string
}

variable "namespace_id" {
  type = number
}