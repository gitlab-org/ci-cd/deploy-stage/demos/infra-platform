resource "random_string" "key_prefix" {
  length           = 8
  special          = false
}

# Generate a password for cosign
resource "random_password" "cosign_password" {
  length           = 16
  special          = false
}

# Generate the cosign keys and store the public key in a SealedSecret
resource "local_file" "cosign_key" {
  provisioner "local-exec" {
    command = "COSIGN_PASSWORD=${random_password.cosign_password.result} cosign generate-key-pair --output-key-prefix ${random_string.key_prefix.result}"
  }

  content  = ""
  filename = "${path.root}/${random_string.key_prefix.result}.pub"
}

# Read the private key
data "local_file" "cosign_key" {
  filename = "${path.root}/${random_string.key_prefix.result}.key"

  depends_on = [
    local_file.cosign_key
  ]
}

# Read the public key
data "local_file" "cosign_pub_key" {
  filename = "${path.root}/${random_string.key_prefix.result}.pub"

  depends_on = [
    local_file.cosign_key
  ]
}

output "key_prefix" {
    value = random_string.key_prefix.result
}

output "private_key" {
    value = data.local_file.cosign_key.content
    sensitive = true
}

output "password" {
    value = random_password.cosign_password.result
    sensitive = true
}

output "public_key" {
    value = data.local_file.cosign_pub_key.content
}

output "public_file" {
  value = data.local_file.cosign_pub_key.filename
}