variable "command" {
    type = string
}

resource "random_string" "filename" {
  length           = 8
  special          = false
}

resource "local_file" "write_file" {
  provisioner "local-exec" {
    command = "${var.command} >> ${path.module}/${random_string.filename.result}"
  }

  content  = ""
  filename = "${path.module}/${random_string.filename.result}"
}

data "local_file" "file" {
  filename = "${path.module}/${random_string.filename.result}"

  depends_on = [
    local_file.write_file
  ]
}

output "content" {
    value = data.local_file.file.content
    sensitive = true
}

output "path" {
    value = data.local_file.file.filename
}
