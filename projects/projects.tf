locals {
  projects = yamldecode(file("./projects.yaml"))
}

module "projects" {
  source = "./modules/projects"

  for_each = local.projects

  name = each.value.name
  path = each.key
  namespace_id = data.gitlab_project.this.namespace_id
  description = each.value.description
  pages_access_level = each.value.pages_access_level
  visibility_level = each.value.visibility_level
  shared_runners_enabled = each.value.shared_runners_enabled
  wiki_enabled = each.value.wiki_enabled
  snippets_enabled = each.value.snippets_enabled
  container_registry_access_level = each.value.container_registry_access_level
  production_agent_slug = join(":", [data.gitlab_project.this.path_with_namespace, "demo-prod"])
  dev_agent_slug = join(":", [data.gitlab_project.this.path_with_namespace, "demo-dev"])
}
