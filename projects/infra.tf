module "infra" {
  source = "./modules/infra"

  for_each = module.projects

  app_project_id = each.value.project_id
  app_project_name = each.value.project_name
  app_project_slug = each.value.project_path_slug
  app_project_full_slug = each.value.path_with_namespace
  namespace = local.projects[each.key].namespace
  gitops_project_id = var.CI_PROJECT_ID
  prod_flux_path = "manifests/prod"
  dev_flux_path = "manifests/dev"
  docker_registry_url = var.CI_REGISTRY
  delivery_docker_credentials = each.value.cluster_token
}
