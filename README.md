# Infrastructure and Platform management with GitLab

This project shows how to set up core infrastructure with GitLab.

It includes three Terraform projects:

- `infra/dev` and `infra/prod` create GKE clusters and set them up for the application delivery and other GitLab features. [Learn more](./docs/infra.md)
- `projects` is an exemplary platform automation that gets its inputs from the [`projects.yaml`](./projects/projects.yaml) file to set up every project for delivery. [Learn more](./docs/platform.md)

The `manifests` project includes the Kubernetes manifests to configure Flux. [Learn more about the installed apps](./docs/apps.md)

The `tools` and `bin` directories are used in GitLab pipelines to generate the container images used in this project's pipelines and in the application delivery pipelines.