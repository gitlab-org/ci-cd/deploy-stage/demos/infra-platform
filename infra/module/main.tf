terraform {
  required_version = "~> 1.0"

  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.3.0"
    }
    flux = {
      source  = "fluxcd/flux"
      version = "1.1.2"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.20"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 4.80"
    }
    kustomization = {
      source  = "kbst/kustomization"
      version = ">= 0.9.5"
    }
  }
}
