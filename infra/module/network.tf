resource "google_compute_network" "custom" {
  name                    = "custom-${var.cluster_name}"
  auto_create_subnetworks = "false" 
  routing_mode            = "GLOBAL"
}

resource "google_compute_subnetwork" "web" {
  name          = "web-${var.cluster_name}"
  ip_cidr_range = "10.10.10.0/24"
  network       = google_compute_network.custom.id
  region        = var.GCP_REGION

  secondary_ip_range  = [
    {
        range_name    = "services"
        ip_cidr_range = "10.10.11.0/24"
    },
    {
        range_name    = "pods"
        ip_cidr_range = "10.1.0.0/20"
    }
  ]

  private_ip_google_access = true
}

resource "google_compute_address" "web" {
  name    = "web-${var.cluster_name}"
  region  = var.GCP_REGION
}

resource "google_compute_router" "web" {
  name    = "web-${var.cluster_name}"
  network = google_compute_network.custom.id
  region  = google_compute_subnetwork.web.region
}

resource "google_compute_router_nat" "web" {
  name                               = "web-${var.cluster_name}"
  router                             = google_compute_router.web.name
  region                             = google_compute_router.web.region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [ google_compute_address.web.self_link ]
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS" 
  subnetwork {
    name                    = google_compute_subnetwork.web.id
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
  depends_on                         = [ google_compute_address.web ]
}

resource "google_compute_firewall" "web" {
  name    = "allow-only-authorized-networks-${var.cluster_name}"
  network = google_compute_network.custom.name

  allow {
    protocol = "tcp"
  }

  priority = 1000

  source_ranges = var.authorized_source_ranges
}
