resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = var.cluster_name
}

resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = "my-agent-token"
  description = "Token for the agent used with `gitlab-agent` Helm Chart"
}

resource "kubernetes_namespace" "agent" {
  metadata {
    name = var.agent_namespace
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_secret" "agent_token" {
  depends_on = [
    kubernetes_namespace.agent
  ]

  metadata {
    name      = "${var.agent_name_cluster}-token"
    namespace = var.agent_namespace
  }

  data = {
    token       = gitlab_cluster_agent_token.this.token
  }
}

resource "gitlab_repository_file" "agent_config" {
  project        = data.gitlab_project.this.id
  branch         = data.gitlab_project.this.default_branch
  file_path      = ".gitlab/agents/${var.cluster_name}/config.yaml"
  content        = base64encode(templatefile("${path.module}/agent_config.yaml.tftpl", { 
    gitlab_namespace_slug=var.CI_PROJECT_NAMESPACE
    }))
  commit_message = "Add agent config"
  overwrite_on_create = true

  depends_on = [kustomization_resource.flux_2]
}

resource "gitlab_repository_file" "agentk" {
  project        = data.gitlab_project.this.id
  branch         = data.gitlab_project.this.default_branch
  file_path      = "${var.flux_path}/agentk.yaml"
  content        = base64encode(templatefile("${path.module}/agentk-helmrelease.tftpl", { 
    agent_name_cluster=var.agent_name_cluster, 
    agent_namespace=var.agent_namespace,
    }))
  commit_message = "Add ${var.flux_path}/agentk.yaml"
  overwrite_on_create = true

  depends_on = [gitlab_repository_file.agent_config]
}
