data "gitlab_project" "this" {
  path_with_namespace = var.gitlab_project_path
}

resource "gitlab_deploy_token" "flux" {
  project    = data.gitlab_project.this.id
  name       = "Flux repository access - ${var.cluster_name}"
  username   = "flux-repository-token"

  scopes = ["read_repository"]
}
