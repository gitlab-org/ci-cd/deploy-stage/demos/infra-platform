data "kustomization_build" "flux" {
  path = "${path.root}/../../manifests/${var.environment}/flux-system"
}

# As we don't store unencrypted sensitive data in manifests, we just need to apply them in order
resource "kustomization_resource" "flux_0" {
  for_each = data.kustomization_build.flux.ids_prio[0]

  manifest = data.kustomization_build.flux.manifests[each.value]

  depends_on = [
    google_container_node_pool.primary_preemptible_nodes
  ]
}

resource "kustomization_resource" "flux_1" {
  for_each = data.kustomization_build.flux.ids_prio[1]

  manifest = data.kustomization_build.flux.manifests[each.value]
  wait = true
  timeouts {
    create = "3m"
    update = "2m"
  }

  depends_on = [
    kustomization_resource.flux_0,
    kubernetes_secret.flux_secret
  ]
}

resource "kustomization_resource" "flux_2" {
  for_each = data.kustomization_build.flux.ids_prio[2]

  manifest = data.kustomization_build.flux.manifests[each.value]

  depends_on = [
    kustomization_resource.flux_1
  ]
}

resource "kubernetes_secret" "flux_secret" {
  depends_on = [
    kustomization_resource.flux_0
  ]

  metadata {
    name      = "flux-system"
    namespace = "flux-system"
  }

  data = {
    username       = "${gitlab_deploy_token.flux.username}"
    password       = "${gitlab_deploy_token.flux.token}"
  }
}