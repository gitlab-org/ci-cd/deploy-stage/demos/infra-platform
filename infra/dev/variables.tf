variable "GCP_PROJECT_ID" {
  type = string
}

variable "GITLAB_TOKEN" {
  type = string
  sensitive = true
}

variable "CI_PROJECT_NAMESPACE" {
  type = string
}

variable "gitlab_project_path" {
  type = string
}

variable "flux_path" {
  description = "flux sync target path"
  type        = string
  default     = "manifests/flux-system"
}

variable "cluster_name" {
  default = "demo"
  description = "Agent name on GitLab, should be a valid directory name to host the configuration file"
}

variable "agent_name_cluster" {
  default = "gitlab-agent"
  description = "Agent name on the cluster"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "GCP_REGION" {
  type = string
  default = "europe-west6"
}

variable "authorized_source_ranges" {
  type        = list(string)
  description = "Addresses or CIDR blocks which are allowed to connect to GKE API Server."
  default = ["0.0.0.0/0"]
}

variable "gke_master_ipv4_cidr_block" {
  type    = string
  default = "172.23.0.0/28"
}
