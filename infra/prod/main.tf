terraform {
  required_version = "~> 1.0"

  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.25"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 4.80"
    }
    kustomization = {
      source  = "kbst/kustomization"
      version = ">= 0.9.5"
    }
  }

  backend "http" {}
}

provider "google" {
  region  = var.GCP_REGION
  project = var.GCP_PROJECT_ID
}

provider "gitlab" {
  token = var.GITLAB_TOKEN
}

data "google_client_config" "default" {}

provider "kubernetes" {
  host = module.main.google_container_cluster.endpoint
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.main.google_container_cluster.master_auth[0].cluster_ca_certificate)
}

provider "kustomization" {
  # Needs a full kubeconfig
  kubeconfig_raw = yamlencode({
    apiVersion = "v1"
    kind = "Config"
    clusters = [{
      cluster = {
        certificate-authority-data = module.main.google_container_cluster.master_auth[0].cluster_ca_certificate
        server = "https://${module.main.google_container_cluster.endpoint}"
      }
      name = "cluster"
    }]
    users = [{
      user = {
        token = data.google_client_config.default.access_token
      }
      name = "user"
    }]
    contexts = [{
      context = {
        cluster = "cluster"
        user = "user"
      }
      name = "context"
    }]
    current-context = "context"
  })
}

module "main" {
    source = "../module"
    
    GCP_PROJECT_ID = var.GCP_PROJECT_ID
    GITLAB_TOKEN = var.GITLAB_TOKEN
    CI_PROJECT_NAMESPACE = var.CI_PROJECT_NAMESPACE
    gitlab_project_path = var.gitlab_project_path
    flux_path = "manifests/prod"
    cluster_name = "demo-prod"
    agent_name_cluster = var.agent_name_cluster
    agent_namespace = var.agent_namespace
    environment = "prod"
    GCP_REGION = "us-east1"
    authorized_source_ranges = var.authorized_source_ranges
    gke_master_ipv4_cidr_block = var.gke_master_ipv4_cidr_block
}
